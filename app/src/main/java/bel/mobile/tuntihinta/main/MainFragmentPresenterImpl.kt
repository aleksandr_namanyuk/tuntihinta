package bel.mobile.tuntihinta.main

import bel.mobile.data.model.Hinta
import bel.mobile.domain.interactor.prices.net.DownloadPrices
import bel.mobile.domain.interactor.prices.db.SavePrices
import bel.mobile.domain.model.HintaModel
import bel.mobile.domain.model.Period.Companion.DIRECTION_BACK
import bel.mobile.domain.model.Period.Companion.DIRECTION_NEXT
import bel.mobile.tuntihinta.Constants.Companion.GRAPH_DATA_COUNT
import com.github.mikephil.charting.data.BarEntry
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableSingleObserver
import kotlinx.android.synthetic.main.fragment_main.*
import org.joda.time.DateTime
import java.util.ArrayList
import javax.inject.Inject

class MainFragmentPresenterImpl @Inject constructor(val view: MainFragmentView): MainFragmentPresenter{

    companion object {
        val TAG = "MainFragmentPresenterImpl"
    }

    override fun bind() {
        view.createBarChart()
        view.setNavigationPanel()
    }

    override fun calculateData(hintaList: List<Hinta>, direction: Int) {
        var max = 0.0
        var sumPriceNoVat = 0.0
        var today = DateTime.now()
        var highlightIndex = -1
        hintaList.forEachIndexed { index, hinta ->

            if(hinta.timestamp.year == today.year &&
                    hinta.timestamp.dayOfYear == today.dayOfYear &&
                    hinta.timestamp.hourOfDay == today.hourOfDay){
                highlightIndex = index
            }

            if(hinta.priceNoVat > max){ max = hinta.priceNoVat }

            sumPriceNoVat += hinta.priceNoVat
        }
        val average = sumPriceNoVat / hintaList.size

//        view.highlightCurrentTime(highlightIndex)
        view.setMaxPrice(max)
        view.setAveragePrice(average)
        view.setBarCharData(createBarDate(hintaList, direction), highlightIndex)
        view.updateXAxisLabel()
    }

    private fun createBarDate(hintaList: List<Hinta>, direction: Int): ArrayList<BarEntry> {
        val data = ArrayList<BarEntry>(GRAPH_DATA_COUNT.toInt())

        if(direction != DIRECTION_BACK ) {
            // if direction is NEXT than show graph from LEFT to -> RIGHT
            hintaList.forEachIndexed { index, hinta -> data.add(BarEntry(index.toFloat(), hinta.priceNoVat.toFloat())) }
        }
        else if(direction == DIRECTION_BACK){
            hintaList.forEachIndexed { index, hinta -> data.add(BarEntry((GRAPH_DATA_COUNT-(hintaList.size-index).toFloat()),
                    hinta.priceNoVat.toFloat())) }
        }

        if(data.size < GRAPH_DATA_COUNT &&
                direction == DIRECTION_NEXT){
            for(i in data.size..GRAPH_DATA_COUNT){
                data.add(BarEntry(i.toFloat(), 0.0f))
            }
        }
        else if(data.size < GRAPH_DATA_COUNT &&
                direction == DIRECTION_BACK){
/*
            // if direction is BACK than show graph from RIGHT to -> LEFT
            hintaList.forEachIndexed {
                index, hinta -> data.add(BarEntry((GRAPH_DATA_COUNT-(data.size - index)).toFloat(),
                    hinta.priceNoVat.toFloat()))
                println("index: $index")
                println("data.size: ${data.size}")
                println("Entry position : ${GRAPH_DATA_COUNT-(data.size - index)}")
            }

            for(i in 0..(GRAPH_DATA_COUNT - data.size)){
                data.add(BarEntry(i.toFloat(), 0.0f))
            }*/
        }

        return data
    }

    override fun unBind(){
    }

}