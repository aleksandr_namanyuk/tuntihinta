package bel.mobile.tuntihinta.main

import bel.mobile.data.model.Hinta
import bel.mobile.tuntihinta.base.BasePresenter

interface MainFragmentPresenter: BasePresenter {

    /**
     * Calculate and update prices for UI TextViews.
     */
    fun calculateData(hintaList: List<Hinta>, direction: Int)
}