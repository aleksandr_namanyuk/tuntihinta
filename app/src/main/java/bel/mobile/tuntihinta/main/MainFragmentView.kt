package bel.mobile.tuntihinta.main

import com.github.mikephil.charting.data.BarEntry
import java.util.ArrayList

interface MainFragmentView{

    /**
     * Init Grapgh with bars to show the prices.
     */
    fun createBarChart()

    /**
     * Insert list of bars with prices to graph and update UI.
     * @param position index of the highlighted bar that represents current time
     */
    fun setBarCharData(barEntryList: ArrayList<BarEntry>, position: Int)

    /**
     * Init panel bar with left and right navigation.
     */
    fun setNavigationPanel()

    /**
     * Update UI for average price textview.
     */
    fun setAveragePrice(average: Double)

    /**
     * Update UI for current price textview.
     */
    fun setCurrentPrice(currentPrice: Double)

    /**
     * Update UI for max price.
     */
    fun setMaxPrice(max: Double)

    /**
     * Update UI for X axis labels on the Graph.
     * */
    fun updateXAxisLabel()

}