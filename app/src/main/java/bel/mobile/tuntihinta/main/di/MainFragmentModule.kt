package bel.mobile.tuntihinta.main.di

import bel.mobile.domain.interactor.prices.net.DownloadPrices
import bel.mobile.domain.interactor.prices.db.SavePrices
import bel.mobile.tuntihinta.main.MainFragment
import bel.mobile.tuntihinta.main.MainFragmentPresenter
import bel.mobile.tuntihinta.main.MainFragmentPresenterImpl
import bel.mobile.tuntihinta.main.MainFragmentView
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class MainFragmentModule{

    @Binds
    abstract fun provideMainFragmentView(mainFragment: MainFragment): MainFragmentView

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun provideMainFragmentPresenter(view: MainFragmentView): MainFragmentPresenter =
                MainFragmentPresenterImpl(view)

    }

}