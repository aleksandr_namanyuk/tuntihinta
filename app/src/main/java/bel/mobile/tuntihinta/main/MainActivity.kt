package bel.mobile.tuntihinta.main

import android.accounts.Account
import android.accounts.AccountManager
import android.content.ContentResolver
import android.graphics.Color
import android.os.Bundle
import android.os.PersistableBundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.view.MenuItem
import bel.mobile.tuntihinta.Constants.Companion.AUTHORITY
import bel.mobile.tuntihinta.Constants.Companion.POLL_FREQUENCY
import bel.mobile.tuntihinta.Constants.Companion.SYNC_TYPE
import bel.mobile.tuntihinta.R
import bel.mobile.tuntihinta.utils.EndDrawerToggle
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import javax.inject.Inject


class MainActivity : DaggerAppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    @Inject lateinit var accountManager: AccountManager

    private var toggle: EndDrawerToggle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
//        supportActionBar?.setDisplayShowTitleEnabled(false)
        title = getString(R.string.app_name)
        toolbar.setTitleTextColor(Color.BLUE)
        toolbar.setSubtitleTextColor(Color.BLUE)

        toggle = EndDrawerToggle(this, drawer_layout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle!!)
        toggle?.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        stubSyncAdapterConnection()
    }

    private fun stubSyncAdapterConnection(){
        val account = Account("stubaccount", SYNC_TYPE)

        if (accountManager.addAccountExplicitly(account, null, null)) {
            ContentResolver.setIsSyncable(account, AUTHORITY, 1);
            ContentResolver.setSyncAutomatically(account, AUTHORITY, true);
            ContentResolver.addPeriodicSync(account, AUTHORITY, Bundle.EMPTY, POLL_FREQUENCY)
//            val bundle = Bundle()
//            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true)
//            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true)
//            ContentResolver.requestSync(account, AUTHORITY, bundle)
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.END)) {
            drawer_layout.closeDrawer(GravityCompat.END)
        } else {
            super.onBackPressed()
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onPostCreate(savedInstanceState, persistentState)
        toggle?.syncState()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
                // Handle the camera action
            }
            R.id.nav_gallery -> {

            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_manage -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.END)
        return true
    }


}
