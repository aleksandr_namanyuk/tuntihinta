package bel.mobile.tuntihinta.main.di

import bel.mobile.tuntihinta.main.MainFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainFragmentBinding{

    @ContributesAndroidInjector(modules = [MainFragmentModule::class])
    abstract fun provideSettingsFragment(): MainFragment
}