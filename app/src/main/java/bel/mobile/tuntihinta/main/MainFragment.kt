package bel.mobile.tuntihinta.main

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import bel.mobile.data.model.Hinta
import bel.mobile.data.model.Hinta_
import bel.mobile.domain.model.Period
import bel.mobile.tuntihinta.Constants.Companion.GRAPH_DATA_COUNT
import bel.mobile.tuntihinta.Constants.Companion.HOURS_IN_DATA_DAY
import bel.mobile.tuntihinta.R
import bel.mobile.tuntihinta.utils.OnSwipeTouchListener
import bel.mobile.tuntihinta.widget.PeriodPicker.Companion.SHIFT_BACK_OFFSET
import bel.mobile.tuntihinta.widget.PeriodPicker.Companion.SHIFT_NEXT_OFFSET
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.listener.BarLineChartTouchListener
import com.github.mikephil.charting.listener.ChartTouchListener
import com.github.mikephil.charting.listener.OnChartGestureListener
import com.trello.rxlifecycle2.components.support.RxFragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import io.objectbox.Box
import io.objectbox.BoxStore
import io.objectbox.android.AndroidScheduler
import io.objectbox.query.Query
import io.objectbox.reactive.DataSubscription
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_main.*
import org.joda.time.DateTime
import java.util.ArrayList
import javax.inject.Inject

class MainFragment: RxFragment(), HasSupportFragmentInjector, MainFragmentView, OnChartGestureListener {

    @Inject lateinit var boxStore: BoxStore
    @Inject lateinit var presenter: MainFragmentPresenter

    private lateinit var hintaBox: Box<Hinta>

    lateinit var currentPriceSubscription: DataSubscription

    var childFragmentInjector: DispatchingAndroidInjector<Fragment>? = null

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? {
        return childFragmentInjector
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        hintaBox = boxStore.boxFor(Hinta::class.java)

        presenter.bind()
    }

    override fun onStart() {
        super.onStart()

        val query: Query<Hinta> = hintaBox.query().equal(Hinta_.timestamp, DateTime.now().withMinuteOfHour(0)
                .withSecondOfMinute(0).withMillisOfSecond(0).millis).build()

        currentPriceSubscription = query.subscribe()
                .transform { it.first() }
                .on(AndroidScheduler.mainThread())
                .observer { setCurrentPrice(it.priceNoVat) }
    }

    override fun onStop() {
        super.onStop()
        currentPriceSubscription.cancel()
    }

    override fun setAveragePrice(average: Double) {
        tvAveragePrice.text = String.format("%.2f c/kWh", average)
    }

    override fun setCurrentPrice(currentPrice: Double) {
        tvPriceNow.text = String.format("%.2f c/kWh", currentPrice)
    }

    override fun setMaxPrice(max: Double) {
        tvMaxPrice.text = String.format("%.2f c/kWh", max)
    }

    // TODO: connect Graph to custom view widget_period_picker
    /* Start of Custom View block */

    override fun createBarChart(){
        priceChart.setDrawBarShadow(false)
        priceChart.setDrawValueAboveBar(true)
        priceChart.setPinchZoom(true)
        priceChart.setDrawGridBackground(false)
        priceChart.description.isEnabled = false
        priceChart.isDoubleTapToZoomEnabled = false
        priceChart.isHighlightPerDragEnabled = false
        priceChart.setScaleEnabled(false)
        priceChart.setFitBars(true)
        priceChart.setPinchZoom(false)
        priceChart.setVisibleXRangeMaximum(48f)
        priceChart.setVisibleXRangeMinimum(48f)
        priceChart.setBackgroundColor(Color.WHITE)

        priceChart.onChartGestureListener = this

        val xAxis = priceChart.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawGridLines(false)
        xAxis.granularity = 24f // only intervals of 1 day
        xAxis.labelCount = 3

        val leftAxis = priceChart.axisLeft
        leftAxis.setLabelCount(3, false)
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
        leftAxis.spaceTop = 15f
        leftAxis.axisMinimum = 0f
        leftAxis.zeroLineWidth = 1f

        val rightAxis = priceChart.axisRight
        rightAxis.isEnabled = false
        val legend = priceChart.legend
        legend.isEnabled = false
    }

    override fun updateXAxisLabel(){
        val timePattern = "HH:mm(dd.MM)"
        val dates = arrayOf(periodPicker.period.startValue.toString(timePattern), periodPicker.period.startValue.plusDays(1).toString(timePattern), periodPicker.period.endTime.toString(timePattern))
        priceChart.xAxis.valueFormatter = XAxisDateFormatter(dates)
    }

    override fun setBarCharData(barEntryList: ArrayList<BarEntry>, position: Int) {
        val set1: BarDataSet

        if (priceChart.data != null && priceChart.data.dataSetCount > 0) {
            set1 = priceChart.data.getDataSetByIndex(0) as BarDataSet
            set1.values = barEntryList
            priceChart.data.notifyDataChanged()
            priceChart.notifyDataSetChanged()
            priceChart.highlightValue(position.toFloat(), priceChart.data.getIndexOfDataSet(set1))
        } else {
            set1 = BarDataSet(barEntryList, "prices")
            set1.setDrawIcons(false)
            set1.highLightColor = Color.RED
            set1.color = Color.BLUE
            set1.setDrawValues(false)

            val dataSets = ArrayList<IBarDataSet>()
            dataSets.add(set1)

            val data = BarData(dataSets)
            data.setValueTextSize(10f)
            data.barWidth = 0.9f
            priceChart.data = data
        }

        priceChart.notifyDataSetChanged()
        priceChart.invalidate()
    }

    override fun setNavigationPanel(){
        val leftNavObserver = periodPicker.leftNavSubscriber
        val rightNavObserver = periodPicker.rightNavSubscriber
        val startObserver = periodPicker.period.startSubscriber

        leftNavObserver.subscribe()
        rightNavObserver.subscribe()

        startObserver
                .map { hintaBox.query()
                        .between(Hinta_.timestamp, it.millis, periodPicker.period.endTime.millis)
                        .build()
                        .find(0, GRAPH_DATA_COUNT)
                }.observeOn(AndroidSchedulers.mainThread())
                .subscribe{
                    presenter.calculateData(it, periodPicker.period.direction)
                }

        leftNavObserver.connect()
        rightNavObserver.connect()
        startObserver.connect()
    }

    /**
     * Formatter class that represent Y axis labels in format of "HH:mm(dd.MM)".
     */
    inner class XAxisDateFormatter(private val values: Array<String>): IAxisValueFormatter {
        override fun getFormattedValue(value: Float, axis: AxisBase?): String {
            if(value/HOURS_IN_DATA_DAY >= values.size) return ""
            return values[value.toInt()/HOURS_IN_DATA_DAY.toInt()]
        }
    }

//    TODO : move gestures to custom view with bar and navigation panel
    /**
     * Graph gesture functions.
     * */

    override fun onChartGestureEnd(me: MotionEvent?, lastPerformedGesture: ChartTouchListener.ChartGesture?) {}

    override fun onChartFling(me1: MotionEvent, me2: MotionEvent, velocityX: Float, velocityY: Float) {
        try {
            val diffY = me2.y - me1.y
            val diffX = me2.x - me1.x
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (Math.abs(diffX) > 100 && Math.abs(velocityX) > 100) {
                    if (diffX < 0) {
                        onSwipeRight()
                    } else {
                        onSwipeLeft()
                    }
                }
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    private fun onSwipeRight(){
        periodPicker.shiftForeward(SHIFT_NEXT_OFFSET)
    }

    private fun onSwipeLeft(){
        periodPicker.shiftBackward(SHIFT_BACK_OFFSET)
    }

    override fun onChartSingleTapped(me: MotionEvent?) {}

    override fun onChartGestureStart(me: MotionEvent?, lastPerformedGesture: ChartTouchListener.ChartGesture?) {}

    override fun onChartScale(me: MotionEvent?, scaleX: Float, scaleY: Float) {}

    override fun onChartLongPressed(me: MotionEvent?) {}

    override fun onChartDoubleTapped(me: MotionEvent?) {}

    override fun onChartTranslate(me: MotionEvent?, dX: Float, dY: Float){}

    /**
     * End Graph gesture functions.
     * */
}