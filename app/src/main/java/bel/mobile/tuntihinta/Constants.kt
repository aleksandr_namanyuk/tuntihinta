package bel.mobile.tuntihinta

import bel.mobile.domain.DomainConstants
import bel.mobile.domain.DomainConstants.Companion.AMOUNT_OF_DAYS_TO_SHOW

class Constants{

    companion object {
        // Data for SyncAdapter
        const val AUTHORITY = "bel.mobile.tuntihinta.provider"
        const val SYNC_TYPE = "bel.mobile.tuntihinta"
//        Download data every 23 hours
        const val POLL_FREQUENCY = 60L * 23L

        const val SHARED_PREFERENCES_NAME = "preferences_tuntihinta"

        const val DEFAULT_PERIOD_IN_HOURS = DomainConstants.DEFAULT_PERIOD_IN_HOURS
        const val HOURS_IN_DATA_DAY = 24L
        const val GRAPH_DATA_COUNT = HOURS_IN_DATA_DAY * AMOUNT_OF_DAYS_TO_SHOW  + 1L
    }
}

