package bel.mobile.tuntihinta.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import bel.mobile.domain.DomainConstants.Companion.AMOUNT_OF_DAYS_TO_SHOW
import bel.mobile.domain.model.Period
import bel.mobile.tuntihinta.R
import com.jakewharton.rxbinding2.view.RxView
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observables.ConnectableObservable
import kotlinx.android.synthetic.main.widget_period_picker.view.*
import org.joda.time.DateTime
import java.util.concurrent.TimeUnit

class PeriodPicker @JvmOverloads constructor(context: Context,
                                             attrs: AttributeSet? = null,
                                             defStyleAttr: Int = 0):
        RelativeLayout(context, attrs, defStyleAttr) {

    var period: Period = Period()

    companion object {
        val PERIOD_DATETIME_UI_PATTERN = "dd.MM.yyyy"
        val WINDOW_DURATION = 250L
        val SHIFT_NEXT_OFFSET = 1
        val SHIFT_BACK_OFFSET = 1
    }

    init {
        LayoutInflater.from(context).inflate(R.layout.widget_period_picker, this, true)
    }

    /**
     * Hot subscriber for the left navigation event.
     * */
    val leftNavSubscriber: ConnectableObservable<Any> =
            RxView.clicks(rootView.navLeftDirection)
                    .bindToLifecycle(this)
                    .throttleFirst(WINDOW_DURATION, TimeUnit.MILLISECONDS)
                    .publish()

    /**
     * Hot subscriber for the right navigation event.
     * */
    val rightNavSubscriber: ConnectableObservable<Any> =
            RxView.clicks(rootView.navRightDirection)
                    .bindToLifecycle(this)
                    .throttleFirst(WINDOW_DURATION, TimeUnit.MILLISECONDS)
                    .publish()

    private val periodStartConnectable: ConnectableObservable<DateTime> =
            period.startSubscriber

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        leftNavSubscriber.subscribe {
            println("leftNavSubscriber.subscribe")
            shiftBackward(SHIFT_BACK_OFFSET)
            changeDateTime()
        }

        rightNavSubscriber.subscribe{
            println("rightNavSubscriber.subscribe")
            shiftForeward(SHIFT_NEXT_OFFSET)
            changeDateTime()
        }

        periodStartConnectable.subscribe{
            period.endTime = it.plusDays(AMOUNT_OF_DAYS_TO_SHOW)
            println("period.startSubscriber")
            changeDateTime()
        }

        initPeriodDate()
        changeDateTime()
    }

    private fun initPeriodDate(){
        period.startValue = DateTime.now().minusDays(1).withHourOfDay(20).withMinuteOfHour(0)
                .withSecondOfMinute(0).withMillisOfSecond(0)
    }

    private fun changeDateTime() {
        rootView.navDatetime.text = context.getString(R.string.navigation_datetime,
                period.startValue.toString(PERIOD_DATETIME_UI_PATTERN), period.endTime.toString(PERIOD_DATETIME_UI_PATTERN))
    }

    fun shiftBackward(days: Int){
        period.startValue = period.startValue.minusDays(days).withHourOfDay(20).withMinuteOfHour(0)
                .withSecondOfMinute(0).withMillisOfSecond(0)
    }

    fun shiftForeward(days: Int){
        period.startValue = period.startValue.plusDays(days).withHourOfDay(20).withMinuteOfHour(0)
                .withSecondOfMinute(0).withMillisOfSecond(0)
    }

}