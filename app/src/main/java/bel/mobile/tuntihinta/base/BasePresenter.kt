package bel.mobile.tuntihinta.base

/**
 * Base presenter with some functionality related to lifecycle events of the view.
 */
interface BasePresenter{

    fun bind()
    fun unBind()
}