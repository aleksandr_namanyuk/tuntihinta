package bel.mobile.tuntihinta.model

import org.joda.time.DateTime

data class HintaView(
        val currentPrice: String,
        val meanPrice: String
)

data class GraphView(val graphTimeline: GraphTimeline,
                     val graphPeriod: GraphPeriod)

class GraphTimeline(
        val startTime: String,
        val currentTime: String,
        val endTime: String
)

class GraphPeriod(
        val startTime: DateTime,
        val endTime: DateTime
)