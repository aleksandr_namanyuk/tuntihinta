package bel.mobile.tuntihinta.syncadapter

import android.app.Service
import android.content.Intent
import android.os.IBinder
import dagger.android.DaggerService
import javax.inject.Inject

class SyncService: DaggerService(){

    @Inject lateinit var syncAdapter: SyncAdapter

    override fun onBind(intent: Intent): IBinder? =
            syncAdapter.syncAdapterBinder

}