package bel.mobile.tuntihinta.syncadapter

import bel.mobile.data.preferences.AppPreferences
import bel.mobile.domain.interactor.prices.net.DownloadHinnatData
import bel.mobile.domain.interactor.prices.net.DownloadPrices
import bel.mobile.domain.interactor.prices.db.SavePrices
import bel.mobile.domain.model.HinnatResponseModel
import bel.mobile.domain.model.HintaModel
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class SyncAdapterPresenterImpl @Inject constructor(val appPreferences: AppPreferences,
                                                   private val downloadHinnatData: DownloadHinnatData,
                                                   val savePrices: SavePrices): SyncAdapterPresenter{

    /**
     * Synchronization Factory function.
     */
    override fun startPerformSync() {
        downloadHinnatData.execute(DownloadPricesDataObserver())
    }

    override fun bind() {
    }

    override fun unbind(){
        savePrices.dispose()
        downloadHinnatData.dispose()
    }

    inner class DownloadPricesDataObserver: DisposableSingleObserver<HinnatResponseModel>(){

        override fun onSuccess(t: HinnatResponseModel) {
            println( "Update Database with new Hinnat ")
            savePrices.execute(t.data, SavePricesObserver())
            appPreferences.saveUpdatedTimestamp(t.updated)
            println("Last updated : ${appPreferences.getUpdatedTimestamp()}")
        }

        override fun onError(e: Throwable) {
            e.printStackTrace()
        }
    }

    inner class SavePricesObserver: DisposableCompletableObserver(){

        override fun onComplete() {
            println("Saveeeeeeeed into the database")
        }

        override fun onError(e: Throwable) {
            e.printStackTrace()
        }
    }
}