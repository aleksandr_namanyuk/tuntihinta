package bel.mobile.tuntihinta.syncadapter

interface SyncAdapterPresenter{

    fun bind()
    fun unbind()
    fun startPerformSync()
}