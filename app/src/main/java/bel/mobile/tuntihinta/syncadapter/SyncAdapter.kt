package bel.mobile.tuntihinta.syncadapter

import android.accounts.Account
import android.content.AbstractThreadedSyncAdapter
import android.content.ContentProviderClient
import android.content.Context
import android.content.SyncResult
import android.os.Bundle
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SyncAdapter @Inject constructor(context: Context,
                                      autoInitialize: Boolean,
                                      private val syncAdapterPresenter: SyncAdapterPresenter):
        AbstractThreadedSyncAdapter(context, autoInitialize, true){

    override fun onPerformSync(account: Account?,
                               extras: Bundle?,
                               authority: String?,
                               provider: ContentProviderClient?,
                               syncResult: SyncResult?) {


        syncAdapterPresenter.startPerformSync()
    }





}