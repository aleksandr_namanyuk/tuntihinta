package bel.mobile.tuntihinta.syncadapter.di

import android.content.Context
import bel.mobile.data.preferences.AppPreferences
import bel.mobile.domain.interactor.prices.net.DownloadHinnatData
import bel.mobile.domain.interactor.prices.net.DownloadPrices
import bel.mobile.domain.interactor.prices.db.SavePrices
import bel.mobile.tuntihinta.di.qualifier.ApplicationContext
import bel.mobile.tuntihinta.syncadapter.SyncAdapter
import bel.mobile.tuntihinta.syncadapter.SyncAdapterPresenter
import bel.mobile.tuntihinta.syncadapter.SyncAdapterPresenterImpl
import dagger.Module
import dagger.Provides

@Module
abstract class SyncAdapterModule{

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun provideSyncAdapter(@ApplicationContext context: Context, syncAdapterPresenter: SyncAdapterPresenter) =
                SyncAdapter(context, true, syncAdapterPresenter)

        @JvmStatic
        @Provides
        fun provideSyncAdapterPresenter(appPreferences: AppPreferences,
                                        downloadHinnatData: DownloadHinnatData,
                                        savePrices: SavePrices): SyncAdapterPresenter =
                SyncAdapterPresenterImpl(appPreferences, downloadHinnatData, savePrices)
    }
}