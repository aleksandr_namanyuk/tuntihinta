package bel.mobile.tuntihinta.syncadapter

import android.content.Intent
import android.os.IBinder
import bel.mobile.data.authenticator.Authenticator
import dagger.android.DaggerService
import javax.inject.Inject

/**
 * A bound Service that instantiates the authenticator when started to synchronise with server.
 */
class AuthenticatorService: DaggerService() {

    @Inject lateinit var mAuthenticator: Authenticator

    override fun onBind(intent: Intent?): IBinder =
            mAuthenticator.iBinder
}