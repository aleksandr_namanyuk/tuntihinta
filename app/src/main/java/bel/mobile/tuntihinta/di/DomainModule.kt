package bel.mobile.tuntihinta.di

import bel.mobile.data.mapper.api.HintaResponseModelMapper
import bel.mobile.data.mapper.database.HintaEntityModelMapper
import bel.mobile.data.mapper.domain.HintaModelMapper
import bel.mobile.data.mapper.domain.ModelHintaMapper
import bel.mobile.data.repository.HintaRepositoryImpl
import bel.mobile.data.repository.source.HintaDataStoreFactory
import bel.mobile.domain.PostExecutionThread
import bel.mobile.domain.ThreadExecutor
import bel.mobile.domain.interactor.prices.net.DownloadPrices
import bel.mobile.domain.interactor.prices.db.SavePrices
import bel.mobile.domain.interactor.prices.net.DownloadHinnatData
import bel.mobile.domain.repository.HintaRepository
import dagger.Module
import dagger.Provides

@Module
class DomainModule{

    @Provides
    fun provideHintaRepository(factory: HintaDataStoreFactory,
                               entityModelMapper: HintaEntityModelMapper,
                               modelMapper: HintaModelMapper,
                               modelHintaMapper: ModelHintaMapper,
                               responseModelMapper: HintaResponseModelMapper): HintaRepository =
            HintaRepositoryImpl(factory, entityModelMapper, modelMapper, modelHintaMapper, responseModelMapper)


    @Provides
    fun provideDownloadPrices(hintaRepository: HintaRepository,
                              threadExecutor: ThreadExecutor,
                              postExecutionThread: PostExecutionThread) =
            DownloadPrices(hintaRepository, threadExecutor, postExecutionThread)

    @Provides
    fun provideSavePrices(hintaRepository: HintaRepository,
                          threadExecutor: ThreadExecutor,
                          postExecutionThread: PostExecutionThread) =
            SavePrices(hintaRepository, threadExecutor, postExecutionThread)

    @Provides
    fun provideDownloadPricesData(hintaRepository: HintaRepository,
                                  threadExecutor: ThreadExecutor,
                                  postExecutionThread: PostExecutionThread) =
            DownloadHinnatData(hintaRepository, threadExecutor, postExecutionThread)

}