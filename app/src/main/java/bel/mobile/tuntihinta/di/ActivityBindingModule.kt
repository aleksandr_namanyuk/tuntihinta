package bel.mobile.tuntihinta.di

import bel.mobile.tuntihinta.main.MainActivity
import bel.mobile.tuntihinta.main.di.MainActivityModule
import bel.mobile.tuntihinta.main.di.MainFragmentBinding
import bel.mobile.tuntihinta.syncadapter.AuthenticatorService
import bel.mobile.tuntihinta.syncadapter.SyncService
import bel.mobile.tuntihinta.syncadapter.di.SyncAdapterModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [MainActivityModule::class, MainFragmentBinding::class])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [SyncAdapterModule::class])
    abstract fun bindSyncAdapter(): SyncService

    @ContributesAndroidInjector
    abstract fun bindAuthenticatorService(): AuthenticatorService
}