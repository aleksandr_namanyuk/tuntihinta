package bel.mobile.tuntihinta.di

import android.content.Context
import android.content.SharedPreferences
import bel.mobile.data.mapper.api.HintaResponseModelMapper
import bel.mobile.data.mapper.database.HintaEntityModelMapper
import bel.mobile.data.mapper.domain.HintaModelMapper
import bel.mobile.data.mapper.domain.ModelHintaMapper
import bel.mobile.data.model.MyObjectBox
import bel.mobile.data.network.RestApiService
import bel.mobile.data.preferences.AppPreferences
import bel.mobile.data.repository.source.HintaCacheDataStoreImpl
import bel.mobile.data.repository.source.HintaDataStoreFactory
import bel.mobile.data.repository.source.HintaRemoteDataStore
import bel.mobile.tuntihinta.Constants.Companion.SHARED_PREFERENCES_NAME
import bel.mobile.tuntihinta.di.qualifier.ApplicationContext
import bel.mobile.tuntihinta.di.scope.ApplicationScope
import dagger.Module
import dagger.Provides
import io.objectbox.BoxStore
import javax.inject.Singleton

@Module
class DataModule{

    @Provides
    @Singleton
    fun provideObjectBox(@ApplicationContext context: Context): BoxStore =
            MyObjectBox.builder().androidContext(context).build()

    @Provides
    @Singleton
    fun provideSharedPreferences(@ApplicationContext context: Context): SharedPreferences =
            context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun provideAppPreferences(sharedPreferences: SharedPreferences) =
            AppPreferences(sharedPreferences)

    @Provides
    @Singleton
    fun provideRetrofit() =
            RestApiService.create()

    @Provides
    fun provideHintaCacheDataStore(boxStore: BoxStore) =
            HintaCacheDataStoreImpl(boxStore)

    @Provides
    fun provideHintaRemoteDataStore(restApiService: RestApiService) =
            HintaRemoteDataStore(restApiService)

    @Provides
    fun provideHintaDataStoreFactory(hintaCacheDataStore: HintaCacheDataStoreImpl,
                                     hintaRemoteDataStore: HintaRemoteDataStore) =
            HintaDataStoreFactory(hintaCacheDataStore, hintaRemoteDataStore)

    @Provides
    fun provideHintaEntityModelMapper() =
            HintaEntityModelMapper()

    @Provides
    fun provideHintaModelMapper() =
            HintaModelMapper()

    @Provides
    fun provideModelHintaMapper() =
            ModelHintaMapper()

    @Provides
    fun provideHintaResponseModelMapper(hintaEntityModelMapper: HintaEntityModelMapper) =
            HintaResponseModelMapper(hintaEntityModelMapper)

}