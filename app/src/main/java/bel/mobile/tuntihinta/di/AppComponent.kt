package bel.mobile.tuntihinta.di

import bel.mobile.tuntihinta.TuntihintaApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    DomainModule::class,
    DataModule::class,
    ActivityBindingModule::class])
interface AppComponent : AndroidInjector<DaggerApplication> {

    @Component.Builder
    interface Builder {
        /**
         * Allows the instance of Application to be injected in the AppComponent component
         * and be used in AppModule without injecting it to the constructor.
         */
        @BindsInstance
        fun application(application: TuntihintaApplication): Builder

        fun build(): AppComponent
    }

    fun inject(application: TuntihintaApplication)

    override fun inject(instance: DaggerApplication)
}