package bel.mobile.tuntihinta.di

import android.accounts.AccountManager
import android.content.Context
import android.content.SharedPreferences
import bel.mobile.data.authenticator.Authenticator
import bel.mobile.data.executor.JobExecutor
import bel.mobile.data.preferences.AppPreferences
import bel.mobile.domain.PostExecutionThread
import bel.mobile.domain.ThreadExecutor
import bel.mobile.tuntihinta.TuntihintaApplication
import bel.mobile.tuntihinta.UIThread
import bel.mobile.tuntihinta.di.qualifier.ApplicationContext
import bel.mobile.tuntihinta.di.scope.ApplicationScope
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    @ApplicationContext
    fun provideApplicationContext(application: TuntihintaApplication): Context =
            application.applicationContext

    @Provides
    @Singleton
    fun provideJobExecutor() = JobExecutor()

    @Provides
    @Singleton
    fun provideUIThread() = UIThread()

    @Provides
    @Singleton
    fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor =
            jobExecutor

    @Provides
    @Singleton
    fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread =
            uiThread

    @Provides
    @Singleton
    fun provideUserAuthenticator(@ApplicationContext  context: Context) =
            Authenticator(context)

    @Provides
    @Singleton
    fun provideAccountManager(@ApplicationContext context: Context) =
            context.getSystemService(Context.ACCOUNT_SERVICE) as AccountManager

}
