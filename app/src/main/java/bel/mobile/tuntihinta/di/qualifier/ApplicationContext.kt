package bel.mobile.tuntihinta.di.qualifier

import javax.inject.Qualifier

/**
 * Context of the activity. Should be used for internal scopes.
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationContext