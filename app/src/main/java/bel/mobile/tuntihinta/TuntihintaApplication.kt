package bel.mobile.tuntihinta

import bel.mobile.tuntihinta.di.AppComponent
import bel.mobile.tuntihinta.di.DaggerAppComponent
import com.squareup.leakcanary.LeakCanary
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import net.danlew.android.joda.JodaTimeAndroid

class TuntihintaApplication: DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<DaggerApplication> {
        val applicationComponent: AppComponent = DaggerAppComponent.builder()
                .application(this)
                .build()
        applicationComponent.inject(this)
        return applicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        JodaTimeAndroid.init(this)
        installLeakCanary()
    }

    private fun installLeakCanary(){
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
        }
        LeakCanary.install(this)
    }
}