package bel.mobile.domain.interactor.base.core

import bel.mobile.domain.PostExecutionThread
import bel.mobile.domain.ThreadExecutor
import bel.mobile.domain.interactor.base.CompletableInteractorWithParameter
import io.reactivex.Completable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.schedulers.Schedulers

abstract class CompletableUseCaseWithParameter<in Param> (private val threadExecutor: ThreadExecutor,
                                                          private val postExecutionThread: PostExecutionThread):
        UseCase(), CompletableInteractorWithParameter<Param> {

    abstract fun createCompletable(param: Param): Completable

    override fun execute(param: Param, completableObserver: DisposableCompletableObserver) {
        compositeDisposable.add(createCompletable(param)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
                .subscribeWith(completableObserver))
    }

    override fun execute(param: Param): Completable =
            createCompletable(param)
}