package bel.mobile.domain.interactor.base

import io.reactivex.Completable
import io.reactivex.observers.DisposableCompletableObserver

interface CompletableInteractor{

    fun execute(completable: DisposableCompletableObserver)
    fun execute(): Completable
}