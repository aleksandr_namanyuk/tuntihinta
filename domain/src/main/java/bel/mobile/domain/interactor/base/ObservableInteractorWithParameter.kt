package bel.mobile.domain.interactor.base

import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver

interface ObservableInteractorWithParameter<in Param, R>{

    fun execute(parameter: Param, observer: DisposableObserver<R>)
    fun execute(parameter: Param): Observable<R>
}