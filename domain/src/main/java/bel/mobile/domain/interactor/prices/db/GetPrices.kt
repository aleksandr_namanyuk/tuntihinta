package bel.mobile.domain.interactor.prices.db

import bel.mobile.domain.PostExecutionThread
import bel.mobile.domain.ThreadExecutor
import bel.mobile.domain.interactor.base.core.SingleUseCase
import bel.mobile.domain.model.HintaModel
import bel.mobile.domain.repository.HintaRepository
import io.reactivex.Single

class GetPrices(private val hintaRepository: HintaRepository,
                threadExecutor: ThreadExecutor,
                postExecutionThread: PostExecutionThread):
        SingleUseCase<MutableList<HintaModel>>(threadExecutor, postExecutionThread) {

    override fun createSingle(): Single<MutableList<HintaModel>> {
       return hintaRepository.getHinnat()
    }
}