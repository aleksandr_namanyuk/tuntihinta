package bel.mobile.domain.interactor.prices.db

import bel.mobile.domain.PostExecutionThread
import bel.mobile.domain.ThreadExecutor
import bel.mobile.domain.interactor.base.core.CompletableUseCaseWithParameter
import bel.mobile.domain.model.HintaModel
import bel.mobile.domain.repository.HintaRepository
import io.reactivex.Completable

class SavePrices(private val hintaRepository: HintaRepository,
                 threadExecutor: ThreadExecutor,
                 postExecutionThread: PostExecutionThread):
        CompletableUseCaseWithParameter<MutableList<HintaModel>>(threadExecutor, postExecutionThread){

    override fun createCompletable(param: MutableList<HintaModel>): Completable =
            hintaRepository.saveHinnat(param)

}