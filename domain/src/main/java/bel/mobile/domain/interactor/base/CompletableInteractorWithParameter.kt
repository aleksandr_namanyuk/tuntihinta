package bel.mobile.domain.interactor.base

import io.reactivex.Completable
import io.reactivex.observers.DisposableCompletableObserver

interface CompletableInteractorWithParameter<in Param>{

    fun execute(param: Param, completableObserver: DisposableCompletableObserver)
    fun execute(param: Param): Completable
}