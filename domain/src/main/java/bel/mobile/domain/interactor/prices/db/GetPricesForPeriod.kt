package bel.mobile.domain.interactor.prices.db

import bel.mobile.domain.PostExecutionThread
import bel.mobile.domain.ThreadExecutor
import bel.mobile.domain.interactor.base.core.SingleUseCaseWithParameter
import bel.mobile.domain.model.HintaModel
import bel.mobile.domain.model.Period
import bel.mobile.domain.repository.HintaRepository
import io.reactivex.Single

class GetPricesForPeriod(private val hintaRepository: HintaRepository,
                         threadExecutor: ThreadExecutor,
                         postExecutionThread: PostExecutionThread):
        SingleUseCaseWithParameter<Period, MutableList<HintaModel>>(threadExecutor, postExecutionThread) {

    override fun createSingle(param: Period): Single<MutableList<HintaModel>> =
            hintaRepository.getHinnat(param)
}