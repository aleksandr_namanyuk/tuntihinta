package bel.mobile.domain.interactor.base.core

import io.reactivex.disposables.CompositeDisposable

abstract class UseCase{

    protected val compositeDisposable = CompositeDisposable()

    fun dispose() = compositeDisposable.clear()
}