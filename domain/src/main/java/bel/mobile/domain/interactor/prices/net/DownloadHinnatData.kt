package bel.mobile.domain.interactor.prices.net

import bel.mobile.domain.PostExecutionThread
import bel.mobile.domain.ThreadExecutor
import bel.mobile.domain.interactor.base.core.SingleUseCase
import bel.mobile.domain.model.HinnatResponseModel
import bel.mobile.domain.repository.HintaRepository
import io.reactivex.Single

class DownloadHinnatData(private val hintaRepository: HintaRepository,
                         threadExecutor: ThreadExecutor,
                         postExecutionThread: PostExecutionThread):
        SingleUseCase<HinnatResponseModel>(threadExecutor, postExecutionThread) {

    override fun createSingle(): Single<HinnatResponseModel> =
            hintaRepository.downloadHinnatData()

}