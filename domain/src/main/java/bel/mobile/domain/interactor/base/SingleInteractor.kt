package bel.mobile.domain.interactor.base

import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver

interface SingleInteractor<R>{

    fun execute(singleObserver: DisposableSingleObserver<R>)
    fun execute(): Single<R>
}