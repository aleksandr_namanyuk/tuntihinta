package bel.mobile.domain.interactor.base.core

import bel.mobile.domain.PostExecutionThread
import bel.mobile.domain.ThreadExecutor
import bel.mobile.domain.interactor.base.SingleInteractor
import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

abstract class SingleUseCase<Return> (private val threadExecutor: ThreadExecutor,
                                 private val postExecutionThread: PostExecutionThread):
        UseCase(), SingleInteractor<Return>
{

    abstract fun createSingle(): Single<Return>

    override fun execute(singleObserver: DisposableSingleObserver<Return>) {
        compositeDisposable.add(createSingle()
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
                .subscribeWith(singleObserver))
    }

    override fun execute(): Single<Return> =
            createSingle()

}