package bel.mobile.domain.interactor.base.core

import bel.mobile.domain.PostExecutionThread
import bel.mobile.domain.ThreadExecutor
import bel.mobile.domain.interactor.base.SingleInteractorWithParameter
import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

abstract class SingleUseCaseWithParameter<in Param, Return> (private val threadExecutor: ThreadExecutor,
                                                        private val postExecutionThread: PostExecutionThread):
        UseCase(), SingleInteractorWithParameter<Param, Return> {

    abstract fun createSingle(param: Param): Single<Return>

    override fun execute(param: Param, singleObserver: DisposableSingleObserver<Return>) {
        compositeDisposable.add(createSingle(param)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
                .subscribeWith(singleObserver))
    }

    override fun execute(param: Param): Single<Return> =
            createSingle(param)

}