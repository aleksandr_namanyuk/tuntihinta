package bel.mobile.domain.interactor.prices.net

import bel.mobile.domain.PostExecutionThread
import bel.mobile.domain.ThreadExecutor
import bel.mobile.domain.interactor.base.core.SingleUseCase
import bel.mobile.domain.model.HintaModel
import bel.mobile.domain.repository.HintaRepository
import io.reactivex.Single

class DownloadPrices(private val hintaRepository: HintaRepository,
                     threadExecutor: ThreadExecutor,
                     postExecutionThread: PostExecutionThread):
        SingleUseCase<MutableList<HintaModel>>(threadExecutor, postExecutionThread) {

    override fun createSingle(): Single<MutableList<HintaModel>> =
            hintaRepository.downloadHinnat()

}