package bel.mobile.domain.interactor.base.core

import bel.mobile.domain.PostExecutionThread
import bel.mobile.domain.ThreadExecutor
import bel.mobile.domain.interactor.base.ObservableInteractorWithParameter
import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

/**
 * Abstract UseCase for specified Data <T>, that takes parameter <Param> as input.
 */
abstract class ObservableUseCaseWithParameter<in Param, R>(private val threadExecutor: ThreadExecutor,
                                                           private val postExecutionThread: PostExecutionThread):
        UseCase(), ObservableInteractorWithParameter<Param, R> {

    abstract fun createObservable(parameter: Param): Observable<R>

    override fun execute(parameter: Param, observer: DisposableObserver<R>) {
        compositeDisposable.add(createObservable(parameter)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
                .subscribeWith(observer))
    }

    override fun execute(parameter: Param): Observable<R> =
            createObservable(parameter)
}