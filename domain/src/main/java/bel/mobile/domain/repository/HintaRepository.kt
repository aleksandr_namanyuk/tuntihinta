package bel.mobile.domain.repository

import bel.mobile.domain.model.HinnatResponseModel
import bel.mobile.domain.model.HintaModel
import bel.mobile.domain.model.Period
import io.reactivex.Completable
import io.reactivex.Single

interface HintaRepository{

    fun getHinnat(): Single<MutableList<HintaModel>>
    fun getHinnat(period: Period): Single<MutableList<HintaModel>>
    fun downloadHinnatData(): Single<HinnatResponseModel>
    fun downloadHinnat(): Single<MutableList<HintaModel>>
    fun downloadHinnatUpdateTime(): Single<String>
    fun saveHinnat(hinnat: MutableList<HintaModel>): Completable
}
