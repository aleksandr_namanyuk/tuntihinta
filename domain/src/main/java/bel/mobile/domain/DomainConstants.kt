package bel.mobile.domain

class DomainConstants{

    companion object {
        const val DEFAULT_PERIOD_IN_HOURS = 48
        const val AMOUNT_OF_DAYS_TO_SHOW = 2
    }
}