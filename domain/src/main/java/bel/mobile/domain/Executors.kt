package bel.mobile.domain

import io.reactivex.Scheduler
import java.util.concurrent.Executor

/**
 * Executor implementation can be based on different frameworks or techniques of asynchronous
 * execution, but every implementation will execute the
 * [bel.mobile.domain.interactor.base.core.UseCase] out of the UI thread.
 */
interface ThreadExecutor: Executor


/**
 * Thread abstraction created to change the execution context from any thread to any other thread.
 * Useful to encapsulate a UI Thread for example, since some job will be done in background, an
 * implementation of this interface will change context and update the UI.
 */
interface PostExecutionThread {
    val scheduler: Scheduler
}