package bel.mobile.domain.model

/**
 * Data model for the Price object used in Domain level to provide  
 */
data class HintaModel @JvmOverloads constructor(
        val index: Long? = 0,
        val timestamp: Long?,
        val priceNoVat: String,
        val priceWithVat: String
)