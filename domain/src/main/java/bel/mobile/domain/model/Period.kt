package bel.mobile.domain.model

import bel.mobile.domain.DomainConstants.Companion.AMOUNT_OF_DAYS_TO_SHOW
import bel.mobile.domain.DomainConstants.Companion.DEFAULT_PERIOD_IN_HOURS
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observables.ConnectableObservable
import io.reactivex.subjects.BehaviorSubject
import org.joda.time.DateTime

data class Period @JvmOverloads constructor(var startTime: DateTime = DateTime.now().minusHours(DEFAULT_PERIOD_IN_HOURS),
                                            var endTime: DateTime = DateTime.now()
){

    companion object {
        const val DIRECTION_NO_CHANGE = -1
        const val DIRECTION_BACK: Int = 0
        const val DIRECTION_NEXT: Int = 1
    }

    var direction: Int = -1

    var startValue: DateTime = DateTime.now()
        set(value){
            direction = when {
                value.isAfter(startValue.millis) -> DIRECTION_NEXT
                value.dayOfMonth == startValue.dayOfMonth -> DIRECTION_NO_CHANGE
                else -> DIRECTION_BACK
            }
            println("old $startValue")
            println("new $value")
            println("direction $direction")
            field = value
            endTime = value.plusDays(AMOUNT_OF_DAYS_TO_SHOW)
            startSubject.onNext(value)
        }

    private val startSubject = BehaviorSubject.create<DateTime>()

    val startSubscriber: ConnectableObservable<DateTime> = startSubject.map { it }.doOnNext {
        println("startSubscriber $it")
    }.publish()

}