package bel.mobile.domain.model

data class HinnatResponseModel(
        val updated: String,
        val data: MutableList<HintaModel>
)