package bel.mobile.domain.interactor;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import bel.mobile.domain.PostExecutionThread;
import bel.mobile.domain.ThreadExecutor;
import bel.mobile.domain.interactor.prices.db.GetPricesForPeriod;
import bel.mobile.domain.model.HintaModel;
import bel.mobile.domain.model.Period;
import bel.mobile.domain.repository.HintaRepository;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetPricesForPeriodTest {

    @Mock HintaRepository mockHintaRepository;
    @Mock
    ThreadExecutor mockThreadExecutor;
    @Mock
    PostExecutionThread mockPostExecutionThread;

    private GetPricesForPeriod getPricesForPeriod;

    @Before
    public void setUp() throws Exception {
        getPricesForPeriod = new GetPricesForPeriod(mockHintaRepository, mockThreadExecutor, mockPostExecutionThread);
    }

    @Test
    public void testGetPricesSuccessfully() throws Exception {

        final HintaModel hintaModel1 = new HintaModel(null, new DateTime("2018-03-22T04:00:00Z").getMillis(),
                "3.859000", "4.785160");
        final HintaModel hintaModel2 = new HintaModel(null, new DateTime("2018-03-22T04:00:00Z").getMillis(),
                "3.859000", "4.785160");
        final HintaModel hintaModel3 = new HintaModel(null, new DateTime("2018-03-22T04:00:00Z").getMillis(),
                "3.859000", "4.785160");

        final List<HintaModel> hintaModelList = new ArrayList<>();
        hintaModelList.add(hintaModel1);
        hintaModelList.add(hintaModel2);
        hintaModelList.add(hintaModel3);

        final DateTime startTime = new DateTime().withYear(2018).withMonthOfYear(2).withDayOfMonth(10);
        final DateTime endTime = startTime.minusDays(14);


        final Period period = new Period(startTime, endTime);

        when(mockHintaRepository.getHinnat(period)).thenReturn(Single.just(hintaModelList));

        final TestObserver testObserver = getPricesForPeriod.createSingle(period).test();
        testObserver.assertComplete();

        verify(mockHintaRepository).getHinnat(period);
        Mockito.verifyNoMoreInteractions(mockHintaRepository);
        Mockito.verifyZeroInteractions(mockThreadExecutor);
        Mockito.verifyZeroInteractions(mockPostExecutionThread);
    }
}
