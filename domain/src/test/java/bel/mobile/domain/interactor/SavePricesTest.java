package bel.mobile.domain.interactor;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import bel.mobile.domain.PostExecutionThread;
import bel.mobile.domain.ThreadExecutor;
import bel.mobile.domain.interactor.prices.db.SavePrices;
import bel.mobile.domain.model.HintaModel;
import bel.mobile.domain.repository.HintaRepository;
import io.reactivex.Completable;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SavePricesTest {

    @Mock private HintaRepository mockHintaRepository;
    @Mock private ThreadExecutor mockThreadExecutor;
    @Mock private PostExecutionThread mockExecutionThread;

    private SavePrices savePricesTest;
    private TestObserver testObserver;

    @Before
    public void setUp() throws Exception {
        testObserver = new TestObserver();
        savePricesTest = new SavePrices(mockHintaRepository, mockThreadExecutor, mockExecutionThread);
    }

    @Test
    public void testSavePricesSuccessfully() throws Exception {
        final HintaModel hintaModel1 = new HintaModel(new DateTime("2018-03-22T04:00:00Z").getMillis(),
                "3.859000", "4.785160");
        final HintaModel hintaModel2 = new HintaModel(new DateTime("2018-03-22T04:00:00Z").getMillis(),
                "3.859000", "4.785160");
        final HintaModel hintaModel3 = new HintaModel(new DateTime("2018-03-22T04:00:00Z").getMillis(),
                "3.859000", "4.785160");

        final List<HintaModel> hintaModelList = new ArrayList<>();
        hintaModelList.add(hintaModel1);
        hintaModelList.add(hintaModel2);
        hintaModelList.add(hintaModel3);

        when(mockHintaRepository.saveHinnat(hintaModelList)).thenReturn(Completable.complete());

        savePricesTest.createCompletable(hintaModelList).subscribe(testObserver);

        testObserver.assertComplete();
    }
}
