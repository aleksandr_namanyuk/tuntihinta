package bel.mobile.domain.interactor;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import bel.mobile.domain.PostExecutionThread;
import bel.mobile.domain.ThreadExecutor;
import bel.mobile.domain.interactor.prices.net.DownloadPrices;
import bel.mobile.domain.model.HinnatResponseModel;
import bel.mobile.domain.model.HintaModel;
import bel.mobile.domain.repository.HintaRepository;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DownloadPricesTest{

    @Mock private HintaRepository mockHintaRepository;
    @Mock private ThreadExecutor mockThreadExecutor;
    @Mock private PostExecutionThread mockExecutionThread;

    private DownloadPrices downloadPrices;

    @Before
    public void setUp() throws Exception {
        downloadPrices = new DownloadPrices(mockHintaRepository, mockThreadExecutor, mockExecutionThread);
    }

    @Test
    public void testDownloadPricesSuccessfully() throws Exception {
        final HintaModel hintaModel1 = new HintaModel(new DateTime("2018-03-22T04:00:00Z").getMillis(),
                "3.859000", "4.785160");
        final HintaModel hintaModel2 = new HintaModel(new DateTime("2018-03-22T04:00:00Z").getMillis(),
                "3.859000", "4.785160");
        final HintaModel hintaModel3 = new HintaModel(new DateTime("2018-03-22T04:00:00Z").getMillis(),
                "3.859000", "4.785160");

        final List<HintaModel> hintaModelList = new ArrayList<>();
        hintaModelList.add(hintaModel1);
        hintaModelList.add(hintaModel2);
        hintaModelList.add(hintaModel3);

        when(mockHintaRepository.downloadHinnat()).thenReturn(Single.just(hintaModelList));
        final TestObserver testObserver = downloadPrices.createSingle().test();
        verify(mockHintaRepository).downloadHinnat();

        Mockito.verifyNoMoreInteractions(mockHintaRepository);
        Mockito.verifyZeroInteractions(mockThreadExecutor);
        Mockito.verifyZeroInteractions(mockExecutionThread);

        testObserver.assertComplete();
        testObserver.assertNoErrors();
    }

}
