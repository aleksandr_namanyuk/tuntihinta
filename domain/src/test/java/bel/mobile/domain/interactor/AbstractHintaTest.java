package bel.mobile.domain.interactor;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import bel.mobile.domain.PostExecutionThread;
import bel.mobile.domain.ThreadExecutor;
import bel.mobile.domain.interactor.base.core.UseCase;
import bel.mobile.domain.repository.HintaRepository;
import io.reactivex.observers.TestObserver;

@RunWith(MockitoJUnitRunner.class)
abstract public class AbstractHintaTest {

    @Mock
    HintaRepository mockHintaRepository;

    @Mock
    ThreadExecutor mockThreadExecutor;

    @Mock
    PostExecutionThread mockExecutionThread;

    UseCase useCase;
}
