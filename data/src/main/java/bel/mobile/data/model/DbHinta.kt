package bel.mobile.data.model

import io.objectbox.annotation.Convert
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.annotation.Uid
import io.objectbox.converter.PropertyConverter
import org.joda.time.DateTime

@Entity
data class Hinta @JvmOverloads constructor(
        @Id var id: Long? = 0,
        @Uid(1699428193223473238L)
        @Convert(converter = TimestampConverter::class, dbType = Long::class)
        val timestamp: DateTime,
        val priceNoVat: Double,
        val priceWithVat: Double
)
class TimestampConverter: PropertyConverter<DateTime, Long> {

    override fun convertToDatabaseValue(entityProperty: DateTime): Long? =
            entityProperty.millis

    override fun convertToEntityProperty(databaseValue: Long?): DateTime =
            DateTime(databaseValue)
}

