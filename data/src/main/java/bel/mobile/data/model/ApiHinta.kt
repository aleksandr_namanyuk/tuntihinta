package bel.mobile.data.model

import com.google.gson.annotations.SerializedName

/**
 * Collection of classes related to API object of the Price(fi: Hinta).
 */


/**
 * API object response that is coming from server.
 */
data class HinnatResponse(
    val updated: String,
    val data: MutableList<HintaEntity>
)


/**
 * API object of Price from json.
 * */
data class HintaEntity(
        val timestamp: String,
        @SerializedName("PriceNoVat") val priceNoVat: String,
        @SerializedName("PriceWithVat") val priceWithVat: String
)