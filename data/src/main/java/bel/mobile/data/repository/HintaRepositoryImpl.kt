package bel.mobile.data.repository

import bel.mobile.data.mapper.api.HintaResponseModelMapper
import bel.mobile.data.mapper.database.HintaEntityModelMapper
import bel.mobile.data.mapper.domain.HintaModelMapper
import bel.mobile.data.mapper.domain.ModelHintaMapper
import bel.mobile.data.model.HinnatResponse
import bel.mobile.data.repository.source.HintaDataStoreFactory
import bel.mobile.domain.model.HinnatResponseModel
import bel.mobile.domain.model.HintaModel
import bel.mobile.domain.model.Period
import bel.mobile.domain.repository.HintaRepository
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HintaRepositoryImpl @Inject constructor(private val factory: HintaDataStoreFactory,
                                              private val entityModelMapper: HintaEntityModelMapper,
                                              private val modelMapper: HintaModelMapper,
                                              private val modelHintaMapper: ModelHintaMapper,
                                              private val responseModelMapper: HintaResponseModelMapper): HintaRepository {

    override fun getHinnat(): Single<MutableList<HintaModel>> {
        return factory.retrieveCacheDataStore().getHintaList()
                .map { modelMapper.transform(it) }
    }

    override fun getHinnat(period: Period): Single<MutableList<HintaModel>> {
        return factory.retrieveCacheDataStore().getHintaList(period)
                .map { modelMapper.transform(it) }
    }

    override fun downloadHinnatData(): Single<HinnatResponseModel> {
        return downloadHinnatResponse()
                .map { responseModelMapper.transform(it) }
    }

    override fun downloadHinnat(): Single<MutableList<HintaModel>> {
        return downloadHinnatResponse().map { it.data }
                .map { entityModelMapper.transform(it) }
    }

    override fun downloadHinnatUpdateTime(): Single<String> {
        return downloadHinnatResponse().map { it.updated }
    }

    private fun downloadHinnatResponse(): Single<HinnatResponse> {
        return factory.retrieveRemoteDataStore().getHinnatResponse()
    }

    override fun saveHinnat(hinnat: MutableList<HintaModel>): Completable {
        val hintaList = modelHintaMapper.transform(hinnat)
        return factory.retrieveCacheDataStore().saveHintaList(hintaList)
    }
}