package bel.mobile.data.repository.source

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HintaDataStoreFactory @Inject constructor(
        private val hintaCacheDataStore: HintaCacheDataStore,
        private val hintaRemoteDataStore: HintaRemoteDataStore){

    fun retrieveCacheDataStore(): HintaCacheDataStore =
            hintaCacheDataStore

    fun retrieveRemoteDataStore(): HintaRemoteDataStore =
            hintaRemoteDataStore
}