package bel.mobile.data.repository.source

import bel.mobile.data.model.HinnatResponse
import bel.mobile.domain.model.Period
import io.reactivex.Completable
import io.reactivex.Single
import org.joda.time.DateTime

interface HintaDataStore<T> {

    fun saveHintaList(hintaList: MutableList<T>): Completable
    fun getHinnatResponse(): Single<HinnatResponse>
    fun getHintaList(period: Period): Single<MutableList<T>>
}