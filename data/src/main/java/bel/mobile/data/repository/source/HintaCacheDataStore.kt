package bel.mobile.data.repository.source

import bel.mobile.data.model.Hinta
import bel.mobile.domain.model.Period
import io.reactivex.Completable
import io.reactivex.Single

interface HintaCacheDataStore{

    fun saveHintaList(hintaList: MutableList<Hinta>): Completable
    fun getHintaList(): Single<MutableList<Hinta>>
    fun getHintaList(period: Period): Single<MutableList<Hinta>>
//    fun saveUpdatedTimestamp(updated: String): Completable
//    fun getUpdatedTimestamp(): Single<String>
}