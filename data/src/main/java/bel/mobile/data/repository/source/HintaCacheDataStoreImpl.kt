package bel.mobile.data.repository.source

import bel.mobile.data.model.Hinta
import bel.mobile.data.model.Hinta_
import bel.mobile.domain.model.Period
import io.objectbox.BoxStore
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class HintaCacheDataStoreImpl @Inject constructor(boxStore: BoxStore): HintaCacheDataStore {

    private val hintaBox = boxStore.boxFor(Hinta::class.java)

    override fun saveHintaList(hintaList: MutableList<Hinta>): Completable {
        hintaList.forEach {
            val exists = hintaBox.query().equal(Hinta_.timestamp, it.timestamp.millis).build().findFirst()
            if(exists == null){ hintaBox.put(it) }
        }
        return Completable.complete()
    }

    override fun getHintaList(): Single<MutableList<Hinta>> =
            Single.fromCallable { hintaBox.query().orderDesc(Hinta_.timestamp).build().find() }

    override fun getHintaList(period: Period): Single<MutableList<Hinta>> =
            Single.fromCallable { hintaBox.query().orderDesc(Hinta_.timestamp)
                    .between(Hinta_.timestamp, period.startTime.toDate(), period.endTime.toDate())
                    .build().find()
            }

//    override fun saveUpdatedTimestamp(updated: String): Completable =
//        Completable.fromCallable{ appPreferences.saveUpdatedTimestamp(updated) }
//
//    override fun getUpdatedTimestamp(): Single<String> =
//            Single.fromCallable { appPreferences.getUpdatedTimestamp() }

    //    private fun convertTime(time: DateTime): String =
//            ISODateTimeFormat.

}

