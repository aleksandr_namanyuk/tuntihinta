package bel.mobile.data.repository.source

import bel.mobile.data.model.HinnatResponse
import bel.mobile.data.model.HintaEntity
import bel.mobile.data.network.RestApiService
import bel.mobile.domain.model.Period
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class HintaRemoteDataStore @Inject constructor(private val api: RestApiService): HintaDataStore<HintaEntity> {

//    lateinit var response : Single<HinnatResponse>

    override fun saveHintaList(hintaList: MutableList<HintaEntity>): Completable =
        throw UnsupportedOperationException()

    override fun getHinnatResponse(): Single<HinnatResponse>{
        return api.getHinnat()
    }

    override fun getHintaList(period: Period): Single<MutableList<HintaEntity>> =
        throw UnsupportedOperationException()

}