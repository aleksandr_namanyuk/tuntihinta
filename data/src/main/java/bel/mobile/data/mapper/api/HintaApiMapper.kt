package bel.mobile.data.mapper.api

import bel.mobile.data.mapper.BaseMapper
import bel.mobile.data.mapper.database.HintaEntityModelMapper
import bel.mobile.data.model.HinnatResponse
import bel.mobile.data.model.Hinta
import bel.mobile.data.model.HintaEntity
import bel.mobile.domain.model.HinnatResponseModel
import org.joda.time.DateTime
import java.lang.UnsupportedOperationException

class HintaApiMapper: BaseMapper<HintaEntity, Hinta> {

    override fun transform(from: HintaEntity): Hinta =
            Hinta(timestamp = DateTime(from.timestamp), priceNoVat = from.priceNoVat.toDouble(), priceWithVat = from.priceWithVat.toDouble())

    override fun transform(from: MutableList<HintaEntity>): MutableList<Hinta> {
        val hintaList = ArrayList<Hinta>(from.size)
        return from.mapTo(hintaList) { transform(it) }
    }
}

class HintaResponseModelMapper(private val hintaEntityMapper: HintaEntityModelMapper): BaseMapper<HinnatResponse, HinnatResponseModel>{

    override fun transform(from: HinnatResponse): HinnatResponseModel =
            HinnatResponseModel(from.updated, hintaEntityMapper.transform(from.data))

    override fun transform(from: MutableList<HinnatResponse>): MutableList<HinnatResponseModel> =
            throw UnsupportedOperationException()
}