package bel.mobile.data.mapper

interface BaseMapper<From, To>{

    fun transform(from: From): To
    fun transform(from: MutableList<From>): MutableList<To>
}