package bel.mobile.data.mapper.domain

import bel.mobile.data.mapper.BaseMapper
import bel.mobile.data.model.Hinta
import bel.mobile.domain.model.HintaModel
import org.joda.time.DateTime

class HintaModelMapper: BaseMapper<Hinta, HintaModel> {

    override fun transform(from: Hinta): HintaModel =
            HintaModel(from.id, from.timestamp.millis, from.priceNoVat.toString(), from.priceWithVat.toString())

    override fun transform(from: MutableList<Hinta>): MutableList<HintaModel> =
            from.mapTo(ArrayList(from.size)) { transform(it) }
}

class ModelHintaMapper: BaseMapper<HintaModel, Hinta> {

    override fun transform(from: HintaModel): Hinta =
            Hinta(from.index, DateTime(from.timestamp), from.priceNoVat.toDouble(), from.priceWithVat.toDouble())

    override fun transform(from: MutableList<HintaModel>): MutableList<Hinta> =
            from.mapTo(ArrayList(from.size)){ transform(it) }
}