package bel.mobile.data.mapper.database

import bel.mobile.data.mapper.BaseMapper
import bel.mobile.data.model.HintaEntity
import bel.mobile.domain.model.HintaModel
import org.joda.time.DateTime

class HintaEntityModelMapper: BaseMapper<HintaEntity, HintaModel> {

    override fun transform(from: HintaEntity): HintaModel =
            HintaModel(timestamp = DateTime(from.timestamp).millis, priceNoVat = from.priceNoVat, priceWithVat = from.priceWithVat)

    override fun transform(from: MutableList<HintaEntity>): MutableList<HintaModel> =
            from.mapTo(ArrayList(from.size)) { transform(it) }

}