package bel.mobile.data.preferences

import android.content.SharedPreferences
import org.joda.time.DateTime
import javax.inject.Inject

class AppPreferences @Inject constructor(private val sharedPreferences: SharedPreferences){

    companion object {
        const val HINTA_UPDATED_TIMESTAMP = "hinta_updated_timestamp"
    }

    fun saveUpdatedTimestamp(updated: String){
        sharedPreferences.edit().putString(HINTA_UPDATED_TIMESTAMP, updated).apply()
    }

    fun getUpdatedTimestamp(): String =
            sharedPreferences.getString(HINTA_UPDATED_TIMESTAMP, DateTime.now().toString())

}