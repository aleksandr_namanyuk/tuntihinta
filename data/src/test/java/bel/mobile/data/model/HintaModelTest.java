package bel.mobile.data.model;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import bel.mobile.data.AbstractObjectTest;
import io.objectbox.Box;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class HintaModelTest extends AbstractObjectTest {

    private Box<Hinta> hintaBox;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        hintaBox = getHintaBox();
    }

    @Test
    public void testHintaModelMainConstructor() throws Exception {
        final Hinta hinta = new Hinta(0L, new DateTime("2018-03-21T22:00:00Z"),
                3.706000, 4.595440);
        hintaBox.put(hinta);
        final Hinta hintaFromBox = hintaBox.query().build().findFirst();

        assertEquals(1, (long) hintaFromBox.getId());
        assertEquals(new DateTime("2018-03-21T22:00:00Z"), hintaFromBox.getTimestamp());
        assertEquals(3.706000 , hintaFromBox.getPriceNoVat(), 0.0);
        assertEquals(4.595440 , hintaFromBox.getPriceWithVat(), 0.0);
    }

    @Test
    public void testHintaModelSecondConstructor() throws Exception {
        final Hinta hinta1 = new Hinta(0L, new DateTime("2018-03-21T22:00:00Z"), 3.706000, 4.595440);
        final Hinta hinta2 = new Hinta(0L, new DateTime("2018-03-21T22:00:00Z"), 3.706000, 4.595440);

        final List<Hinta> hintaList = new ArrayList<>();
        hintaList.add(hinta1);
        hintaList.add(hinta2);

        hintaBox.put(hintaList);

        final List<Hinta> hintaListFromBox = hintaBox.query().build().find();

        final Hinta hintaFromBox1 = hintaListFromBox.get(0);
        final Hinta hintaFromBox2 = hintaListFromBox.get(1);

        assertEquals(2, hintaListFromBox.size());
        assertEquals(new DateTime("2018-03-21T22:00:00Z"), hintaFromBox1.getTimestamp());
        assertEquals(new DateTime("2018-03-21T22:00:00Z"), hintaFromBox2.getTimestamp());

        assertEquals(3.706000, hintaFromBox1.getPriceNoVat(), 0.0);
        assertEquals(3.706000, hintaFromBox2.getPriceNoVat(), 0.0);

        assertEquals(4.595440, hintaFromBox1.getPriceWithVat(), 0.0);
        assertEquals(4.595440, hintaFromBox2.getPriceWithVat(), 0.0);
    }

    @Test
    public void testHintaModelSearch(){
        insertDummyHintaListIntoDb();

        final List<Hinta> hintaListFromBox = hintaBox.query().equal(Hinta_.timestamp, new DateTime("2018-03-26T14:00:00Z").getMillis()).build().find();
        assertEquals(1, hintaListFromBox.size());

        final Hinta firstHintaByDate = hintaBox.query().equal(Hinta_.timestamp, new DateTime("2018-03-26T14:00:00Z").getMillis()).build().findUnique();
        assertNotNull(firstHintaByDate);
        assertEquals(18L, (long) firstHintaByDate.getId());

        final List<Hinta> hintaListFromBoxFromCurrentDate = hintaBox.query().greater(Hinta_.id, firstHintaByDate.getId())
                .build().find(0, 24);

        assertEquals(12, hintaListFromBoxFromCurrentDate.size());

        double max = 0;
        double sumPriceNoVat = 0;
        for (Hinta h : hintaListFromBoxFromCurrentDate) {
            sumPriceNoVat += h.getPriceNoVat();

            if(h.getPriceNoVat() > max) max = h.getPriceNoVat();
        }

        double average = sumPriceNoVat / hintaListFromBoxFromCurrentDate.size();

        assertEquals(5.348000, max, 0.0);
        assertEquals(4.407000, average, 0.1);
    }

    @Test
    public void testHintaConverterData(){

        DateTime dateTime = formatTimestamp("2018-03-26T14:00:00Z");

        assertEquals(2018, dateTime.getYear());
        assertEquals(3, dateTime.getMonthOfYear());
        assertEquals(26, dateTime.getDayOfMonth());
        assertEquals(14, dateTime.getHourOfDay());
        assertEquals(0, dateTime.getMinuteOfHour());
    }

    @Test
    public void testHintaConverterWithApiValues(){

        DateTime dateTime = formatTimestamp("2018-03-26T14:00:00Z");
        assertEquals(2018, dateTime.getYear());
        assertEquals(3, dateTime.getMonthOfYear());
        assertEquals(26, dateTime.getDayOfMonth());
        assertEquals(14, dateTime.getHourOfDay());
        assertEquals(0, dateTime.getMinuteOfHour());

        DateTime nextDay = DateTime.now().plusDays(1).withHourOfDay(20);
        assertEquals(2018, dateTime.getYear());
        assertEquals(3, dateTime.getMonthOfYear());
    }

    private DateTime formatTimestamp(String timestamp){
        return DateTime.parse(timestamp, DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'"));
    }

    private void insertDummyHintaListIntoDb() {
        Hinta hinta1 = new Hinta(0L, new DateTime("2018-03-25T21:00:00Z"), 3.954000, 4.902960);
        Hinta hinta2 = new Hinta(0L, new DateTime("2018-03-25T22:00:00Z"), 4.036000, 5.004640);
        Hinta hinta3 = new Hinta(0L, new DateTime("2018-03-25T23:00:00Z"), 3.953000, 4.901720);
        Hinta hinta4 = new Hinta(0L, new DateTime("2018-03-26T00:00:00Z"), 3.892000, 4.826080);
        Hinta hinta5 = new Hinta(0L, new DateTime("2018-03-26T01:00:00Z"), 3.801000, 4.713240);
        Hinta hinta6 = new Hinta(0L, new DateTime("2018-03-26T02:00:00Z"), 3.934000, 4.878160);
        Hinta hinta7 = new Hinta(0L, new DateTime("2018-03-26T03:00:00Z"), 4.165000, 5.164600);
        Hinta hinta8 = new Hinta(0L, new DateTime("2018-03-26T04:00:00Z"), 4.589000, 5.690360);
        Hinta hinta9 = new Hinta(0L, new DateTime("2018-03-26T05:00:00Z"), 5.160000, 6.398400);
        Hinta hinta10 = new Hinta(0L, new DateTime("2018-03-26T06:00:00Z"), 6.381000, 7.912440);
        Hinta hinta11 = new Hinta(0L, new DateTime("2018-03-26T07:00:00Z"), 6.759000, 8.381160);
        Hinta hinta12 = new Hinta(0L, new DateTime("2018-03-26T08:00:00Z"), 5.793000, 7.183320);
        Hinta hinta13 = new Hinta(0L, new DateTime("2018-03-26T09:00:00Z"), 5.488000, 6.805120);
        Hinta hinta14 = new Hinta(0L, new DateTime("2018-03-26T10:00:00Z"), 5.197000, 6.444280);
        Hinta hinta15 = new Hinta(0L, new DateTime("2018-03-26T11:00:00Z"), 5.054000, 6.266960);
        Hinta hinta16 = new Hinta(0L, new DateTime("2018-03-26T12:00:00Z"), 4.792000, 5.942080);
        Hinta hinta17 = new Hinta(0L, new DateTime("2018-03-26T13:00:00Z"), 4.534000, 5.622160);
        Hinta hinta18 = new Hinta(0L, new DateTime("2018-03-26T14:00:00Z"), 4.403000, 5.459720);
        Hinta hinta19 = new Hinta(0L, new DateTime("2018-03-26T15:00:00Z"), 4.509000, 5.591160);
        Hinta hinta20 = new Hinta(0L, new DateTime("2018-03-26T16:00:00Z"), 4.955000, 6.144200);
        Hinta hinta21 = new Hinta(0L, new DateTime("2018-03-26T17:00:00Z"), 5.348000, 6.631520);
        Hinta hinta22 = new Hinta(0L, new DateTime("2018-03-26T18:00:00Z"), 4.806000, 5.959440);
        Hinta hinta23 = new Hinta(0L, new DateTime("2018-03-26T19:00:00Z"), 4.534000, 5.622160);
        Hinta hinta24 = new Hinta(0L, new DateTime("2018-03-26T20:00:00Z"), 4.364000, 5.411360);
        Hinta hinta25 = new Hinta(0L, new DateTime("2018-03-26T21:00:00Z"), 4.210000, 5.220400);
        Hinta hinta26 = new Hinta(0L, new DateTime("2018-03-26T22:00:00Z"), 4.240000, 5.257600);
        Hinta hinta27 = new Hinta(0L, new DateTime("2018-03-26T23:00:00Z"), 4.015000, 4.978600);
        Hinta hinta28 = new Hinta(0L, new DateTime("2018-03-27T00:00:00Z"), 3.975000, 4.929000);
        Hinta hinta29 = new Hinta(0L, new DateTime("2018-03-27T01:00:00Z"), 3.953000, 4.901720);
        Hinta hinta30 = new Hinta(0L, new DateTime("2018-03-27T02:00:00Z"), 3.986000, 4.942640);

        final List<Hinta> hintaList = new ArrayList<>();
        hintaList.add(hinta1);
        hintaList.add(hinta2);
        hintaList.add(hinta3);
        hintaList.add(hinta4);
        hintaList.add(hinta5);
        hintaList.add(hinta6);
        hintaList.add(hinta7);
        hintaList.add(hinta8);
        hintaList.add(hinta9);
        hintaList.add(hinta10);
        hintaList.add(hinta11);
        hintaList.add(hinta12);
        hintaList.add(hinta13);
        hintaList.add(hinta14);
        hintaList.add(hinta15);
        hintaList.add(hinta16);
        hintaList.add(hinta17);
        hintaList.add(hinta18);
        hintaList.add(hinta19);
        hintaList.add(hinta20);
        hintaList.add(hinta21);
        hintaList.add(hinta22);
        hintaList.add(hinta23);
        hintaList.add(hinta24);
        hintaList.add(hinta25);
        hintaList.add(hinta26);
        hintaList.add(hinta27);
        hintaList.add(hinta28);
        hintaList.add(hinta29);
        hintaList.add(hinta30);

        hintaBox.put(hintaList);
    }
}
