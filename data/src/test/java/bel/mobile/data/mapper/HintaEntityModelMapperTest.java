package bel.mobile.data.mapper;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import bel.mobile.data.mapper.database.HintaEntityModelMapper;
import bel.mobile.data.model.HintaEntity;
import bel.mobile.domain.model.HintaModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class HintaEntityModelMapperTest {


    HintaEntityModelMapper hintaEntityModelMapper;

    @Before
    public void setUp() throws Exception {
        hintaEntityModelMapper = new HintaEntityModelMapper();
    }

    @Test
    public void testFromEntityToModelMapper() throws Exception {
        HintaEntity hintaEntity = new HintaEntity("2018-03-21T22:00:00Z", "3.706000", "4.595440");

        HintaModel hintaModel = hintaEntityModelMapper.transform(hintaEntity);

        assertNotNull(hintaModel);
        assertNotNull(hintaModel.getIndex());
        assertEquals(0L, (long) hintaModel.getIndex());
        assertEquals(new DateTime("2018-03-21T22:00:00Z").getMillis(), hintaModel.getTimestamp(), 0.0);
        assertEquals("3.706000", hintaModel.getPriceNoVat());
        assertEquals("4.595440", hintaModel.getPriceWithVat());
    }
}
