package bel.mobile.data.mapper;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import bel.mobile.data.mapper.api.HintaApiMapper;
import bel.mobile.data.model.Hinta;
import bel.mobile.data.model.HintaEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class HintaApiMapperTest {

    private HintaApiMapper hintaApiMapper;

    @Before
    public void setUp() throws Exception {
        hintaApiMapper = new HintaApiMapper();
    }

    @Test
    public void testTransformFromApiModelToDbModel() throws Exception {
        HintaEntity hintaEntity = new HintaEntity("2018-03-21T22:00:00Z", "3.706000", "4.595440");
        Hinta hinta = hintaApiMapper.transform(hintaEntity);

        assertNotNull(hinta.getId());
        assertEquals(0L, (long) hinta.getId());
        assertEquals(new DateTime("2018-03-21T22:00:00Z"), hinta.getTimestamp());
        assertEquals(3.706000, hinta.getPriceNoVat(), 0.0);
        assertEquals(4.595440, hinta.getPriceWithVat(), 0.0);
    }

    @Test
    public void testTransformFromApiModelListToDbModelList() throws Exception {
        HintaEntity hinta1 = new HintaEntity("2018-03-21T22:00:00Z", "3.706000", "4.595440");
        HintaEntity hinta2 = new HintaEntity("2018-03-21T23:00:00Z", "3.632000", "4.503680");

        List<HintaEntity> hintaEntityList = new ArrayList<>();

        hintaEntityList.add(hinta1);
        hintaEntityList.add(hinta2);

        List<Hinta> hintaList = hintaApiMapper.transform(hintaEntityList);

        assertEquals(2, hintaList.size());
        assertEquals(new DateTime("2018-03-21T22:00:00Z"), hintaList.get(0).getTimestamp());
        assertEquals(new DateTime("2018-03-21T23:00:00Z"), hintaList.get(1).getTimestamp());
        assertEquals(3.706000, hintaList.get(0).getPriceNoVat(), 0.0);
        assertEquals(3.632000, hintaList.get(1).getPriceNoVat(), 0.0);
        assertEquals(4.595440, hintaList.get(0).getPriceWithVat(), 0.0);
        assertEquals(4.503680, hintaList.get(1).getPriceWithVat(), 0.0);


    }
}
