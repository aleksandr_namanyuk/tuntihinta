package bel.mobile.data.repository;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import bel.mobile.data.mapper.api.HintaApiMapper;
import bel.mobile.data.mapper.api.HintaResponseModelMapper;
import bel.mobile.data.mapper.database.HintaEntityModelMapper;
import bel.mobile.data.mapper.domain.HintaModelMapper;
import bel.mobile.data.mapper.domain.ModelHintaMapper;
import bel.mobile.data.model.HinnatResponse;
import bel.mobile.data.model.Hinta;
import bel.mobile.data.model.HintaEntity;
import bel.mobile.data.repository.source.HintaCacheDataStoreImpl;
import bel.mobile.data.repository.source.HintaDataStoreFactory;
import bel.mobile.data.repository.source.HintaRemoteDataStore;
import bel.mobile.domain.model.HinnatResponseModel;
import bel.mobile.domain.model.HintaModel;
import bel.mobile.domain.model.Period;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HintaRepositoryImplTest {

    @Mock
    HintaCacheDataStoreImpl mockHintaCacheDataStore;
    @Mock HintaRemoteDataStore mockHintaRemoteDataStore;
    @Mock HintaDataStoreFactory mockHintaDataStoreFactory;
    @Mock HintaApiMapper mockHintaApiMapper;
    @Mock HintaEntityModelMapper mockHintaEntityModelMapper;
    @Mock HintaModelMapper mockHintaModelMapper;
    @Mock ModelHintaMapper modelHintaMapper;
    @Mock HintaResponseModelMapper mockHintaResponseModelMapper;

    private HintaRepositoryImpl hintaRepository;

    @Before
    public void setUp() throws Exception {
        hintaRepository = new HintaRepositoryImpl(mockHintaDataStoreFactory,
                mockHintaEntityModelMapper, mockHintaModelMapper, modelHintaMapper, mockHintaResponseModelMapper);
    }

    @Test
    public void getHinnatSucceed() throws Exception {
        Hinta hinta1 = new Hinta(1L, new DateTime("2018-03-21T22:00:00Z"), 3.706000, 4.595440);
        Hinta hinta2 = new Hinta(2L, new DateTime("2018-03-21T23:00:00Z"), 3.632000, 4.503680);

        List<Hinta> hintaList = new ArrayList<>();

        hintaList.add(hinta1);
        hintaList.add(hinta2);

        when(mockHintaDataStoreFactory.retrieveCacheDataStore()).thenReturn(mockHintaCacheDataStore);
        when(mockHintaCacheDataStore.getHintaList()).thenReturn(Single.just(hintaList));
        TestObserver testObserver = hintaRepository.getHinnat().test();
        testObserver.assertSubscribed();
        testObserver.assertComplete();
    }

    @Test
    public void downloadHinnatSucceedAndSaveToCache() throws Exception {
        HintaEntity hinta1 = new HintaEntity("2018-03-21T22:00:00Z", "3.706000", "4.595440");
        HintaEntity hinta2 = new HintaEntity("2018-03-21T23:00:00Z", "3.632000", "4.503680");

        List<HintaEntity> hintaList = new ArrayList<>();

        hintaList.add(hinta1);
        hintaList.add(hinta2);

        HinnatResponse hinnatResponse = new HinnatResponse( "2018-03-21T22:00:00Z", hintaList);

        Hinta hinta3 = new Hinta(1L, new DateTime("2018-03-21T22:00:00Z"), 3.706000, 4.595440);
        Hinta hinta4 = new Hinta(2L, new DateTime("2018-03-21T23:00:00Z"), 3.632000, 4.503680);

        List<Hinta> hintaList1 = new ArrayList<>();

        hintaList1.add(hinta3);
        hintaList1.add(hinta4);

        when(mockHintaDataStoreFactory.retrieveRemoteDataStore()).thenReturn(mockHintaRemoteDataStore);
        when(mockHintaRemoteDataStore.getHinnatResponse()).thenReturn(Single.just(hinnatResponse));


        TestObserver testObserver = hintaRepository.downloadHinnat().test();
        testObserver.assertComplete();
    }

    @Test
    public void downloadHinnatResponse() throws Exception {

        HintaEntity hinta1 = new HintaEntity("2018-03-21T22:00:00Z", "3.706000", "4.595440");
        HintaEntity hinta2 = new HintaEntity("2018-03-21T23:00:00Z", "3.632000", "4.503680");
        List<HintaEntity> hintaList = new ArrayList<>();
        hintaList.add(hinta1);
        hintaList.add(hinta2);

        HinnatResponse hinnatResponse = new HinnatResponse( "2018-03-21T22:00:00Z", hintaList);

        HintaModel hintaModel1 = new HintaModel(new DateTime("2018-03-21T22:00:00Z").getMillis(),
                "3.706000", "4.595440");
        HintaModel hintaModel2 = new HintaModel(new DateTime("2018-03-21T23:00:00Z").getMillis(),
                "3.632000", "4.503680");

        List<HintaModel> hintaModels = new ArrayList<>();
        hintaModels.add(hintaModel1);
        hintaModels.add(hintaModel2);

        HinnatResponseModel hinnatResponseModel = new HinnatResponseModel("2018-03-21T22:00:00Z", hintaModels);

        when(mockHintaDataStoreFactory.retrieveRemoteDataStore()).thenReturn(mockHintaRemoteDataStore);
        when(mockHintaResponseModelMapper.transform(hinnatResponse)).thenReturn(hinnatResponseModel);
        when(mockHintaRemoteDataStore.getHinnatResponse()).thenReturn(Single.just(hinnatResponse));

        TestObserver testObserver = new TestObserver();
        hintaRepository.downloadHinnatData().subscribe(testObserver);

        testObserver.assertComplete();

    }
}
