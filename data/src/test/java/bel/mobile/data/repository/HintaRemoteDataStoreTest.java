package bel.mobile.data.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import bel.mobile.data.model.HinnatResponse;
import bel.mobile.data.model.HintaEntity;
import bel.mobile.data.network.RestApiService;
import bel.mobile.data.repository.source.HintaRemoteDataStore;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HintaRemoteDataStoreTest {

    @Mock
    RestApiService mockRestApiService;

    private HintaRemoteDataStore hintaRemoteDataStore;

    @Before
    public void setUp() throws Exception {
        hintaRemoteDataStore = new HintaRemoteDataStore(mockRestApiService);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testUnsupportedMethodSavingPriceList() throws Exception {
        HintaEntity hinta1 = new HintaEntity("2018-03-21T22:00:00Z", "3.706000", "4.595440");
        HintaEntity hinta2 = new HintaEntity("2018-03-21T23:00:00Z", "3.632000", "4.503680");

        List<HintaEntity> hintaList = new ArrayList<>();

        hintaList.add(hinta1);
        hintaList.add(hinta2);

        hintaRemoteDataStore.saveHintaList(hintaList).test();
    }

    @Test
    public void testGetHintaListCompletes() throws Exception {
        HintaEntity hinta1 = new HintaEntity("2018-03-21T22:00:00Z", "3.706000", "4.595440");
        HintaEntity hinta2 = new HintaEntity("2018-03-21T23:00:00Z", "3.632000", "4.503680");
        HintaEntity hinta3 = new HintaEntity("2018-03-22T00:00:00Z", "3.559000", "4.413160");

        List<HintaEntity> hintaList = new ArrayList<>();
        hintaList.add(hinta1);
        hintaList.add(hinta2);
        hintaList.add(hinta3);

        HinnatResponse hinnatResponse = new HinnatResponse("2018-03-27T13:00Z", hintaList);

        when(mockRestApiService.getHinnat()).thenReturn(Single.just(hinnatResponse));

        TestObserver testObserver = hintaRemoteDataStore.getHinnatResponse().test();
        verify(mockRestApiService).getHinnat();

        testObserver.assertComplete();
//        testObserver.assertValue(hintaList);
    }

}
